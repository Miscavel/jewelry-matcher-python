# Jewelry Matcher Python

A web service that gives a list of jewelries ordered by similarity to the input image.

A few images found in [jewel_input](https://gitlab.com/Miscavel/jewelry-matcher-python/tree/master/jewel_input) can be used for testing purposes.

Web service is live at https://jewel-magnifier.herokuapp.com

Example :

![example-image](screenshot/ss1.jpg)