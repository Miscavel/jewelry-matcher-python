import os

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'miscavel-9871123-redox'
