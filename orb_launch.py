import cv2
import numpy as np

lowe_threshold = [0.5, 0.9]
lowe_increment = 0.01
base_img = [1, 2, 3, 4, 5]
answer = [1, 2, 3, 4, 2]
k_value = 2
best_accuracy = 0
best_lowe_ratio = 0

orb = cv2.ORB_create()
bf = cv2.BFMatcher(cv2.NORM_HAMMING2)
accuracy = 0
l = lowe_threshold[0]
while l < lowe_threshold[1]:
    lowe_ratio = l
    accuracy = 0
    print("Lowe ratio : " + str(lowe_ratio))
    for i in range (0, len(base_img)):
        img1 = cv2.imread("instance/jewel_input/jewel_" + str(base_img[i]) + ".jpg")
        img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
        kp1, des1 = orb.detectAndCompute(img1, None)
        score = []
        input_img = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        for j in range (0, len(input_img)):
            img2 = cv2.imread("instance/jewel_train/jewel_" + str(input_img[j]) + ".jpg")
            img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
            kp2, des2 = orb.detectAndCompute(img2, None)

            # Brute Force Matching
            matches = bf.knnMatch(des1, des2, k = k_value)

            good = []

            for m,n in matches:
                if m.distance < lowe_ratio*n.distance:
                    good.append([m])

            score.append(len(good))
            
        for j in range (0, len(score)):
            for k in range (j, len(score)):
                if score[k] > score[j]:
                    temp = score[k]
                    score[k] = score[j]
                    score[j] = temp

                    temp = input_img[k]
                    input_img[k] = input_img[j]
                    input_img[j] = temp
        accuracy += 1/(input_img.index(answer[i]) + 1);

    accuracy = accuracy * 100 / len(base_img)
    print("Accuracy : " + str(accuracy))
    if best_lowe_ratio == 0:
        best_lowe_ratio = lowe_ratio
        best_accuracy = accuracy
    else:
        if accuracy > best_accuracy:
            best_lowe_ratio = lowe_ratio
            best_accuracy = accuracy
    l += lowe_increment
print("Best Lowe ratio : " + str(best_lowe_ratio) + " with accuracy : " + str(best_accuracy) + "%")
loweFile = open("instance/lowe.txt","w+")
loweFile.write(str(best_lowe_ratio) + ";" + str(best_accuracy))
loweFile.close()
