from flask import render_template, flash, redirect, url_for, send_from_directory
from app import app
from app.forms import LoginForm, ImageForm
from werkzeug.utils import secure_filename
from app import orb_predict
import os

@app.route('/')
@app.route('/index')
def index():
    return redirect(url_for('image'))
    user = {'username': 'Miscavel'}
    posts = [
        {
            'author': {'username': 'John'},
            'body': 'Beautiful day in Portland!'
        },
        {
            'author': {'username': 'Susan'},
            'body': 'The Avengers movie was so cool!'
        }
    ]
    return render_template('index.html', title='Home', user=user, posts=posts)

@app.route('/login', methods=['GET', 'POST'])
def login():
    return redirect(url_for('image'))
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In', form=form)

@app.route('/image', methods=['GET', 'POST'])
def image():
    form = ImageForm()
    results = []
    preview = "none"
    if form.validate_on_submit():
        f = form.photo.data
        filename = secure_filename(f.filename)
        f.save(os.path.join(
            app.instance_path, 'jewel_upload', filename
        ))
        results = orb_predict.predict_image(filename, app.instance_path)
        preview = filename
    return render_template('image.html', title='Image', form=form, results=results, preview=preview)

@app.route('/uploads/<path:filename>')
def download_file(filename):
    return send_from_directory(app.instance_path, filename, as_attachment=True)
