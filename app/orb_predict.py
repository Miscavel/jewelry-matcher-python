import cv2
import numpy as np
import sys

def predict_image(image, root):
    base_img = image
    k_value = 2

    loweFile = open(root + "/lowe.txt","r")
    lowe_ratio = loweFile.read()
    lowe_ratio = float(lowe_ratio.split(';')[0])

    orb = cv2.ORB_create()
    bf = cv2.BFMatcher(cv2.NORM_HAMMING2)

    img1 = cv2.imread(root + "/jewel_upload/" + base_img)
    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    kp1, des1 = orb.detectAndCompute(img1, None)

    score = []
    input_img = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    for j in range (0, len(input_img)):
        img2 = cv2.imread(root + "/jewel_train/jewel_" + str(input_img[j]) + ".jpg")
        img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
        kp2, des2 = orb.detectAndCompute(img2, None)

        # Brute Force Matching
        matches = bf.knnMatch(des1, des2, k = k_value)

        good = []

        for m,n in matches:
            if m.distance < lowe_ratio*n.distance:
                good.append([m])

        score.append(len(good))
        
    for j in range (0, len(score)):
        for k in range (j, len(score)):
            if score[k] > score[j]:
                temp = score[k]
                score[k] = score[j]
                score[j] = temp

                temp = input_img[k]
                input_img[k] = input_img[j]
                input_img[j] = temp
    return input_img
